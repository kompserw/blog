package pl.akademiakodu.blog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.akademiakodu.blog.dao.CommentDao;
import pl.akademiakodu.blog.entity.Comment;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDao commentDao;
    @Override
    public List<Comment> findAll() {
        List<Comment> list = new ArrayList<>();
        commentDao.findAll().forEach(list::add);
        return list;
    }

    @Override
    public List<Comment> findForPost(long postId) {
        return commentDao.findCommentsByPostId(postId);
    }

    @Override
    public void save(Comment c) {
        commentDao.save(c);
    }
}
