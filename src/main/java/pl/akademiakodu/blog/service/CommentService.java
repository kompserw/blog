package pl.akademiakodu.blog.service;

import pl.akademiakodu.blog.entity.Comment;

import java.util.List;

public interface CommentService {

    List<Comment> findAll();

    List<Comment> findForPost(long postId);

    void save(Comment c);
}
