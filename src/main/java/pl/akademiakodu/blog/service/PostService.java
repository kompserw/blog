package pl.akademiakodu.blog.service;

import pl.akademiakodu.blog.entity.Post;

import java.util.List;

public interface PostService {

    List<Post> listAll();
}
