package pl.akademiakodu.blog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.akademiakodu.blog.dao.PostDao;
import pl.akademiakodu.blog.entity.Post;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostDao postDao;

    @Override
    public List<Post> listAll() {
        List<Post> list = new ArrayList<>();
        postDao.findAll().forEach(list::add);
        return list;
    }
}
