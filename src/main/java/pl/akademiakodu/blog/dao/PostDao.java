package pl.akademiakodu.blog.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.akademiakodu.blog.entity.Post;

@Repository
public interface PostDao extends CrudRepository<Post, Long> {
}
