package pl.akademiakodu.blog.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "POST")
public class Post {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "AUTHOR")
    private String author;

    @Column(name = "DATE_CREATED", columnDefinition = "TIMESTAMP")
    private LocalDateTime currentDateTime;

    @Column(name = "TITLE")
    private String title;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "content", column =
                @Column(name = "CONTENT", columnDefinition = "TEXT")),
            @AttributeOverride(name = "subcontent", column =
                @Column(name = "SUBCONTENT", columnDefinition = "TEXT"))
    })
    private PostContent postContent;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Comment> comments;

    public Post() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDateTime getCurrentDateTime() {
        return currentDateTime;
    }

    public void setCurrentDateTime(LocalDateTime currentDateTime) {
        this.currentDateTime = currentDateTime;
    }

    public PostContent getPostContent() {
        return postContent;
    }

    public void setPostContent(PostContent postContent) {
        this.postContent = postContent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
