package pl.akademiakodu.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.akademiakodu.blog.entity.Comment;
import pl.akademiakodu.blog.service.CommentService;

import java.util.List;

@Controller
@RequestMapping("/comments/")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @RequestMapping("/post-{id}")
    public ModelAndView getCommentsForPost(@PathVariable("id") long postId) {
        return getCommentListView(postId);
    }


    @PostMapping("/add")
    public ModelAndView saveComment(@ModelAttribute("comment") Comment c) {
        System.out.println(c);
        System.out.println(c.getPost());
        commentService.save(c);
        return new ModelAndView("redirect:/comments/post-" + c.getPost().getId());
    }

    private ModelAndView getCommentListView(long postId) {
        return new ModelAndView("comments/list", "comments",
                commentService.findForPost(postId));
    }
}
