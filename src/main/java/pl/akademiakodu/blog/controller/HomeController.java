package pl.akademiakodu.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.akademiakodu.blog.entity.Comment;
import pl.akademiakodu.blog.service.PostService;

@RequestMapping("/")
@Controller
public class HomeController {

    @Autowired
    private PostService postService;

    @GetMapping
    public ModelAndView getAllPosts() {
        ModelAndView mav = new ModelAndView("posts/list");
        mav.addObject("posts", postService.listAll());
        mav.addObject("comment", new Comment());
        return mav;
    }
}
